"""
Provides transfer, integration, and signal processing functions for a PCI forward model

Calculations are done in the object plane (plane of the plasma), the beam line-of-sight is assumed to be located at the origin/optical axis, the detector elements are assumed to be distributed along the x-direction (but may have a finite extent along the y-direction), the unperturbed beam is taken to be a Gaussian truncated by an aperture stop, the phase plate is taken to have a groove oriented along the y-direction, and the expressions implemented mainly stem from [1]. Physical quantities are in SI units unless explicitly stated otherwise.

[1] S. Coda "An Experimental Study of Turbulence by Phase Contrast Imaging in the DIII-D Tokamak" PhD Thesis (MIT, Cambridge, 1997)

Author:
    - Soeren Kjer Hansen <kjerh@ipp.mpg.de>
"""

import numpy as np
from scipy.signal import convolve, spectrogram, get_window
from scipy.interpolate import interp1d, RectBivariateSpline
from scipy.integrate import simps

def trunc_gauss_1d(x1d,wbeam,aperture):
    '''Gaussian function centred at x = 0 truncated by an aperture stop at x = +/- aperture
    
    Args:
        x1d (1d array): x-values for which the truncated Gaussian is evaluated
        wbeam (float): 1/e field width of the beam
        aperature (float): radius of the aperture stop around x = 0
    
    Returns: truncated Gaussian evaluated on x1d (1d array)

    '''
    return np.heaviside(aperture - np.abs(x1d),0.5)*np.exp(-x1d**2/wbeam**2)

def trunc_gauss_2d(x2d,y2d,wbeam,aperture):
    '''Gaussian function centred at the origin truncated by an aperture stop at r = aperture
    
    Args:
        x2d (2d array): x-values for which the truncated Gaussian is evaluated
        y2d (2d array): y-values for which the truncated Gaussian is evaluated
        wbeam (float): 1/e field width of the beam
        aperature (float): radius of the aperture stop around x = 0
        
    Returns: truncated Gaussian evaluated on (x2d,y2d) (2d array)

    '''
    return np.heaviside(aperture**2 - x2d**2 - y2d**2,0.5)*np.exp(-(x2d**2+y2d**2)/wbeam**2)

def response_k_min_1d(xnd,k_min):
    '''Response function of a phase plate with groove parallel to the y-direction from Eq. (2.123) of [1] 
    (note the lack of the delta function which is implemented by integrating/convolving only along the x-dirction)
    
    Args:
        xnd (nd array): x-values for which the response function is evaluated
        k_min (float): Low-k cutoff of the phase plate
    
    Returns: Response function evaluated on xnd (nd array)
    '''
    return (k_min/np.pi)*np.sinc(k_min*xnd/np.pi)

def weight_func_gaussian_beam_1d(x1d,wbeam,aperture,k_min = 160.44):
    '''Weight functions of a truncated Gaussian from Eqs. (2.134) and (2.149) of [1] evaluated the x-axis
    
    Args:
        Defined above (x1d is assumed to have constant spacing here)
        
    Returns:
        Weight (1d array): Weight function from Eq. (2.134) of [1]
        Conv (1d array): C function from Eq. (2.149) of [1]*
        Conv0 (float): C0 = C(0)
        
    Notes:
        *not scaled by distance between x1d-elements
    '''
    
    # Convolve truncated Gaussian with the phase plate response function along the x-axis
    Conv = convolve(response_k_min_1d(x1d,k_min),trunc_gauss_1d(x1d,wbeam,aperture),mode = 'same')
    
    # Find the value of the convolved function at the beam center (assuming x1d contains x = 0)
    argx0 = np.argmin(x1d**2)
    Conv0 = Conv[argx0]
    
    # Calculate weight from Eq. (2.134) of [1]
    Weight = trunc_gauss_1d(x1d,wbeam,aperture)*Conv/Conv0
    
    return Weight, Conv, Conv0

def weight_func_gaussian_beam_2d(x2d,y2d,wbeam,aperture,k_min = 160.44):
    '''Weight functions of a truncated Gaussian from Eqs. (2.134) and (2.149) of [1] evaluated in 2d
    
    Args:
        Defined above (x2d is assumed to have constant spacing here)
        
    Returns:
        Weight: Weight function from Eq. (2.134) of [1] (2d array)
        Conv: C function from Eq. (2.149) of [1] with the range modified depending on the y-position (2d array)*
        Conv0: C0 = C(0) (float)*
        
    Notes:
       *not scaled by distance between x1d-elements'''
    
    # Define response and truncated Gaussian on the (x1d,y1d)-grid
    k2d = response_k_min_1d(x2d,k_min)
    TruncGauss2d = trunc_gauss_2d(x2d,y2d,wbeam,aperture)
    
    # Pre-allocate Conv
    shape2d = np.shape(k2d)
    Conv = np.zeros(shape2d)
    
    # Convolve truncated Gaussian with the phase plate response function on 2d grid
    for n in range(shape2d[0]):
        Conv[n,:] = convolve(k2d[n,:],TruncGauss2d[n,:],mode = 'same')
    
    # Find the value of the convolved function at the beam center (assuming (x2d,y2d) contains the origin)
    index0 = np.argmin(np.square(x2d)+np.square(y2d))
    arg0 = np.unravel_index(index0,shape2d)
    Conv0 = Conv[arg0]
    
    # Calculate weight from Eq. (2.134) of [1]
    Weight = trunc_gauss_2d(x2d,y2d,wbeam,aperture)*Conv/Conv0
    
    return Weight, Conv, Conv0

def exact_trans_func_gaussian_beam_1d(num_paths,x_phi_tilde,t_phi_tilde,phi_tilde,wbeam,aperture,Power=100,k_min=160.44,reflect=0.28):
    '''Exact transfer function evaluated along the x-axis from Eq. (2.148) of [1]
    
    Args: 
        num_steps (integer): resolution of the signal along the x-axis
        x_phi_tilde (1d array): x-values of the input fluctuating phase shift
        t_phi_tilde (1d array): times at which the input flucutating phase shift is evaluated
        phi_tilde (2d array): input fluctuating phase shift
        wbeam (float): 1/e field width of Gaussian beam
        aperture (float): radius of aperture stop
        Power (float): Gaussian beam power
        k_min (float): low-k cutoff of the phase plate
        reflect (float): reflection coefficient of the central groove of the phase plate
        
    Returns:
        x1d (1d array): array on which the signal is calculated (covers the full aperture opening with step length of x_step)
        Sig (2d array): intensity scaled to the object plane evaluated on the (x1d,t_phi_tilde) grid
        Weight_t (2d array): weight of the signal on the (x1d,t_phi_tilde) grid
    '''
    
    # Define x1d grid
    x1d = np.linspace(-aperture,aperture,num_paths)
    
    # Calculate x step
    x_step = x1d[1] - x1d[0]
    
    # Define response and truncated Gaussian on the x1d-grid
    k1d = response_k_min_1d(x1d,k_min)
    TruncGauss1d = trunc_gauss_1d(x1d,wbeam,aperture)
    
    # Calculate weight, C, and C0 on the x1d-grid
    Weight, Convx, Conv0 = weight_func_gaussian_beam_1d(x1d,wbeam,aperture,k_min)
    
    # Scale C and C0 by the x1d-grid spacing
    Cx = Convx*x_step
    C0 = Conv0*x_step
    
    # Pre-allocate quantities evaluated on the (x1d,t_phi_tilde)-grid
    phi_tilde_1d = np.zeros((len(x1d),len(t_phi_tilde)))
    Weight_t = np.zeros(np.shape(phi_tilde_1d))
    Cx_t = np.zeros(np.shape(phi_tilde_1d))
    Integ_1d = np.zeros(np.shape(phi_tilde_1d))
    
    # Loop over t_phi_tilde
    for n in range(len(t_phi_tilde)):
        # Interpolate phi_tilde to x1d at a specific time point
        phi_tilde_interp = interp1d(x_phi_tilde,phi_tilde[:,n],kind = 'cubic',bounds_error = False)
        phi_tilde_1d[:,n] = phi_tilde_interp(x1d)
        
        # Calcuate weight, C, and the integral from Eq. (2.148) of [1] at a given time point
        Weight_t[:,n] = Weight
        Cx_t[:,n] = Cx
        Integ_1d[:,n] = convolve(k1d,phi_tilde_1d[:,n]*TruncGauss1d,mode = 'same')*x_step
        
    # Calculate the intensity on the (x1d,t_phi_tilde)-grid
    Sig = 4*np.sqrt(reflect)*Power/(np.pi*wbeam**2)*C0*Weight_t*(phi_tilde_1d - Integ_1d/Cx_t)
    
    return x1d, Sig, Weight_t

def exact_trans_func_gaussian_beam_2d(num_paths_x,num_paths_y,x_phi_tilde,y_phi_tilde,t_phi_tilde,phi_tilde,wbeam,aperture,Magnification=5,det_space=0.0005,Power=100,k_min=160.44,reflect=0.28):
    '''Exact transfer function evaluated along the x-axis and the extension of the detector elements along the y-direction from Eq. (2.126) of [1]
    
    Args: 
        num_paths_x (integer): number of paths along the x-direction
        num_paths_y (integer): number of paths along the y-direction
        x_phi_tilde (1d array): x-values of the input fluctuating phase shift
        y_phi_tilde (1d array): y-values of the input fluctuating phase shift
        t_phi_tilde (1d array): times at which the input flucutating phase shift is evaluated
        phi_tilde (2d array): input fluctuating phase shift on the (x_phi_tilde,y_phi_tilde,t_phi_tilde)-grid
        wbeam (float): 1/e field width of Gaussian beam
        aperture (float): radius of aperture stop
        Magnification (float): (object size)/(image size)
        det_space (float): spacing of detectors in the image plane
        Power (float): Gaussian beam power
        k_min (float): low-k cutoff of the phase plate
        reflect (float): reflection coefficient of the central groove of the phase plate
        
    Returns:
        x1d (1d array): x-values for which the signal is calculated (covers the full aperture opening with step length of x_step)
        y1d (1d array): y-values for which the signal is calculated (covers the detector extent along the y-direction, assuming square detector elements)
        Sig (3d array): intensity scaled to the object plane evaluated on the (y1d,x1d,t_phi_tilde)-grid
        Weight_t (3d array): weight of the signal on the (y1d,x1d,t_phi_tilde)-grid
        
    Notes:
        Magnification is defined as (object size)/(image size), which differs from some standard definitions
    '''
    
    # Define x1d and y1d grids
    x1d = np.linspace(-aperture,aperture,num_paths_x)           
    y1d = np.linspace(-0.5*Magnification*det_space,0.5*Magnification*det_space,num_paths_y)
    
    # Calculate x step
    x_step = x1d[1] - x1d[0]
    
    # Calculate meshgrid
    x2d, y2d = np.meshgrid(x1d,y1d)
    
    # Define response and truncated Gaussian on the (x1d,y1d)-grid
    k2d = response_k_min_1d(x2d,k_min)
    TruncGauss2d = trunc_gauss_2d(x2d,y2d,wbeam,aperture)
    
    #Calculate weight, C, and C0 on the (x1d,y1d)-grid
    Weight, Convx, Conv0 = weight_func_gaussian_beam_2d(x2d,y2d,wbeam,aperture,k_min)
    
    # Scale C and C0 by the x1d-grid spacing
    Cx = Convx*x_step
    C0 = Conv0*x_step
    
    # Pre-allocate quantities evaluated on the (y1d,x1d,t_phi_tilde)-grid
    phi_tilde_2d = np.zeros((len(y1d),len(x1d),len(t_phi_tilde)))
    Weight_t = np.zeros(phi_tilde_2d.shape)
    Cx_t = np.zeros(phi_tilde_2d.shape)
    Integ_2d = np.zeros(phi_tilde_2d.shape)
    
    # Loop over t_phi_tilde
    for n in range(len(t_phi_tilde)):
        # Interpolate phi_tilde to (x1d,y1d) at a specific time point
        phi_tilde_interp = RectBivariateSpline(y_phi_tilde,x_phi_tilde,np.transpose(phi_tilde[:,:,n]),kx=2)    
        phi_tilde_2d[:,:,n] = phi_tilde_interp(y1d,x1d)
        
        # Calcuate weight and C at a specific time point
        Weight_t[:,:,n] = Weight
        Cx_t[:,:,n] = Cx
        
        # Calculate the (latter) convolution integral from Eq. (2.126) of [1] for the y1d-values at a given time point
        for m in range(len(y1d)):
            Integ_2d[m,:,n] = convolve(k2d[m,:],phi_tilde_2d[m,:,n]*TruncGauss2d[m,:],mode = 'same')*x_step
    
    # Calculate the intensity on the (y1d,x1d,t_phi_tilde)-grid
    Sig = 4*np.sqrt(reflect)*Power/(np.pi*wbeam**2)*C0*Weight_t*(phi_tilde_2d - Integ_2d/Cx_t)
    
    return x1d, y1d, Sig, Weight_t

def integrated_signal_1d(x1d,t_phi_tilde,Sig,Weight,Magnification=5,det_space=0.0005,det_number=32,responsivity = 6e3):
    '''Integrate the signal calculated along the x-axis over detector elements centred around the beam center
    
    Args:
        x1d (1d array): x-values for which the signal is evaluated
        t_phi_tilde (1d array): time points for which the signal is evaluated
        Sig (2d array): intensity scaled to the object plane evaluated on the (x1d,t_phi_tilde)-grid
        Weight (2d array): weight of the signal on the (x1d,t_phi_tilde)-grid
        Magnification (float): (object size)/(image size)
        det_space (float): detector spacing in the image plane
        det_number (float): number of detectors
        responsivity (float): responsivity of detector element in V/W (defaults to 6e3, corresponding to a 0.5 mm square HgCdTe element of the type JD15D12-M294-S500U-60)
        
    Returns:
        x_ch (1d array): channel x-locations
        Sig_Int (2d array): Signal integrated over the channels on the (x_ch,t_phi_tilde)-grid in V
        Weight_Int (2d array): Weight integrated over the channels on the (x_ch,t_phi_tilde)-grid
        
    Notes:
        Magnification is defined as (object size)/(image size), which differs from some standard definitions
        Signal along the y-direction is assumed constant and integrals are multiplied by the extension of the channels along the y-direction (ch_space), assuming square detector elements
    '''
    
    # Calculate x_ch-grid (in the object plane)
    ch_space = det_space*Magnification
    x_ch_temp = ch_space*np.arange(det_number)
    x_ch = x_ch_temp - x_ch_temp[np.int64(np.ceil(det_number/2)-1)]
    
    # Pre-allocate Sig_Int and Weight_Int
    Sig_Int = np.zeros((det_number,len(t_phi_tilde)))
    Weight_Int = np.zeros(np.shape(Sig_Int))
    
    # Loop over detector elements
    for n in range(det_number):
        # Calculate x-limits of the detector elements
        x_min = x_ch[n] - 0.5*ch_space
        x_max = x_ch[n] + 0.5*ch_space
        arg_min_x = np.argmin((x1d - x_min)**2)
        arg_max_x = np.argmin((x1d - x_max)**2)
        
        # Calculate detector weight integral (only necessary for one time point)               
        Weight0 = simps(Weight[arg_min_x:(arg_max_x+1),0],x1d[arg_min_x:(arg_max_x+1)])
        
        # Loop over t_phi_tilde to calculate the integrated signal
        for m in range(len(t_phi_tilde)):
            Sig_Int[n,m] = simps(Sig[arg_min_x:(arg_max_x+1),m],x1d[arg_min_x:(arg_max_x+1)])
            Weight_Int[n,m] = Weight0
        
    # Multiply signal and weight by the detector extent along the y-direction (assuming square detector elements) and responsivity
    Sig_Int = Sig_Int*ch_space*responsivity
    Weight_Int = Weight_Int*ch_space*responsivity
    
    return x_ch, Sig_Int, Weight_Int

def integrated_signal_2d(x1d,y1d,t1d,Sig,Weight,Magnification=5,det_space=0.0005,det_number=32,responsivity=6e3):
    '''Integrate the signal calculated along the x and y directions over detector elements centred around the beam center
    
    Args:
        x1d (1d array): x-values for which the signal is evaluated
        y1d (1d array): y-values for which the signal is evaluated
        t_phi_tilde (1d array): time points for which the signal is evaluated
        Sig (3d array): intensity scaled to the objected plane evaluated on the (y1d,x1d,t_phi_tilde)-grid
        Weight (3d array): weight of the signal on the (y1d,x1d,t_phi_tilde)-grid
        Magnification (float): (object size)/(image size)
        det_space (float): detector spacing in the image plane
        det_number (float): number of detectors
        responsivity (float): responsivity of detector element in V/W (defaults to 6e3, corresponding to a 0.5 mm square HgCdTe element of the type JD15D12-M294-S500U-60)
        
    Returns:
        x_ch (1d array): channel x-locations
        Sig_Int (2d array): Signal integrated over the channels on the (x_ch,t_phi_tilde)-grid in V
        Weight_Int (2d array): Weight integrated over the channels on the (x_ch,t_phi_tilde)-grid
        
    Notes:
        Magnification is defined as (object size)/(image size), which differs from some standard definitions
        Signal along the y-direction is assumed constant and integrals are multiplied by the extension of the channels along the y-direction (ch_space), assuming square detector elements
    '''
    
    # Calculate x_ch-grid (in the object plane)
    ch_space = det_space*Magnification
    x_ch_temp = ch_space*np.arange(det_number)
    x_ch = x_ch_temp - x_ch_temp[np.int64(np.ceil(det_number/2)-1)]
    
    # Pre-allocate Sig_Int and Weight_Int
    Sig_Int = np.zeros((det_number,len(t1d)))
    Weight_Int = np.zeros(np.shape(Sig_Int))
    
    for n in range(det_number):
        # Calculate x-limits and y-limits of the detector elements
        x_min = x_ch[n] - 0.5*ch_space
        x_max = x_ch[n] + 0.5*ch_space
        arg_min_x = np.argmin((x1d-x_min)**2)
        arg_max_x = np.argmin((x1d-x_max)**2)
        
        arg_min_y = np.argmin((y1d+0.5*ch_space)**2)
        arg_max_y = np.argmin((y1d-0.5*ch_space)**2)
        
        # Calculate detector weight integral (only necessary for one time point)               
        Weight0 = simps(simps(Weight[arg_min_y:(arg_max_y+1),arg_min_x:(arg_max_x+1),0],x1d[arg_min_x:(arg_max_x+1)]),y1d[arg_min_y:(arg_max_y+1)])
        
        # Loop over t_phi_tilde to calculate the integrated signal
        for m in range(len(t1d)):
            Sig_Int[n,m] = simps(simps(Sig[arg_min_y:(arg_max_y+1),arg_min_x:(arg_max_x+1),m],x1d[arg_min_x:(arg_max_x+1)]),y1d[arg_min_y:(arg_max_y+1)])
            Weight_Int[n,m] = Weight0
            
        # Multiply signal and weight by the responsivity
        Sig_Int = Sig_Int*responsivity
        Weight_Int = Weight_Int*responsivity
    
    return x_ch, Sig_Int, Weight_Int

def _k_dft_helper(x, data, window=None, mean=None):
    '''Helper function for kf_spec to calculate k-spectrum of a single data slice.
    Similar to process.calcdft(). By Jan-Peter Bähner <jpb@ipp.mpg.de>

    Args:
        x (1d array): distance vector in m.
        data (2d array, complex): data array (f x t).
        window (str, optional): name of windowing function applied to
        channel array. See scipy.signal.get_window for possible inputs.

        Defaults to None, which means no window is used.
        mean (bool, optional): remove offset over channels before x->k
        transform (suppresses k=0 components).

        Returns:
            k (1d array): wavenumber in cm^-1.
            psd (1d array): power spectral density in V^2 cm / Hz.
        '''
    
    # prepare window and offset
    Nx = len(x)
    window = get_window(window, Nx) if window else 1
    offset = np.mean(data) if mean else 0

    # get dx in cm
    dx = (x[1] - x[0])*100

    # transform
    k_raw = np.fft.fftfreq(Nx, d=dx)
    dft_raw = np.fft.fft(window * (data - offset), n=Nx)

    # shift the wavenumber vector
    k = 2 * np.pi * np.fft.fftshift(k_raw)
    # shift and take absolute value of the DFT data
    psd = np.abs(np.fft.fftshift(dft_raw)) ** 2

    # scaling
    # consider profile on data as additional window function
    data_profile = np.abs(data) / max(np.abs(data)) if max(np.abs(data)) > 0 else 1
    scale = dx / (window ** 2 * data_profile ** 2).sum()
    psd *= scale

    # symmetrize spectrum:
    if Nx % 2 == 0:
        # append first entry
        k = np.append(k, np.abs(k[0]))
        psd = np.append(psd, psd[0])

    return k, psd

def interp_to_regular_time_grid(t_phi_tilde,Sig_Int,Weight_Int,t_start = None,t_stop = None,num_points = None):
    '''Interpolate PCI signals to a regular time grid for calculating spectrograms 
    
    Args:
        t_phi_tilde (1d array): Time points for which the signal is known
        Sig_Int (2d array): Signal integrated over the channels on the (x_ch,t_phi_tilde)-grid
        Weight_Int (2d array): Weight integrated over the channels on the (x_ch,t_phi_tilde)-grid
        t_start (float): Start time point of the interpolation grid (defaults to t_phi_tilde[0])
        t_stop (float): End time point of the interpolation grid (defaults to t_phi_tilde[-1])
        num_points: Number of points in the interpolation grid (defaults to the number of points in t_phi_tilde)
        
    Returns:
        t1d (1d array): Time interpolation grid
        Sig_Interp (2d array): ignal integrated over the channels on the (x_ch,t1d)-grid
        Weight_Interp (2d array): Weight integrated over the channels on the (x_ch,t1d)-grid
    '''
    
    # Define time interpolation grid
    t_start = t_start if t_start else t_phi_tilde[0]
    t_stop = t_stop if t_stop else t_phi_tilde[-1]
    num_points = num_points if num_points else len(t_phi_tilde)    
    t1d = np.linspace(t_start,t_stop,num_points)
    
    # Get number of channels
    lx_ch = Sig_Int.shape[0]
    
    # Pre-allocate Sig_Interp and Weight_Interp
    Sig_Interp = np.zeros((lx_ch,num_points))
    Weight_Interp = np.zeros((lx_ch,num_points))
    
    for n in range(lx_ch):
        # Define interpolation functions
        Sig_interp1d = interp1d(t_phi_tilde,Sig_Int[n,:],kind = 'cubic')
        Weight_interp1d = interp1d(t_phi_tilde,Weight_Int[n,:],kind = 'linear')
        
        # Interpolate to t1d
        Sig_Interp[n,:] = Sig_interp1d(t1d)
        Weight_Interp[n,:] = Weight_interp1d(t1d)
    
    return t1d, Sig_Interp, Weight_Interp

def get_kf_spec(x_ch,t1d,Sig,df = None,**kwargs):
    '''Get (k,f)-spectrum in a manner similar to the experimental ones
    
    Args:
        x_ch (1d array): channel x-locations
        t1d (1d array): regular time grid
        Sig (2d array): Signal on the (x_ch,t1d)-grid
        df (float): frequency resolution (defaults to the highest frequency resolution allowed by t1d-grid)
        **kwargs (optional): keyword arguments for the spectrogram function
        
    Returns:
        k (1d array): k-values of (k,f)-spectra in cm^-1
        f (1d array): f-values of (k,f)-spectra in Hz
        t (1d array): time points of (k,f)-spectra
        s_kf (3d array): power spectral density on (k,f,t)-grid in V^2 cm / Hz
    '''
    
    # Define sampling frequency
    fs = 1/(t1d[1] - t1d[0])
    
    # Define frequency resolution
    df = df if df else 1/(max(t1d) - min(t1d))
    
    # Define overlap between adjacent time segments
    overlap_frac = 0.5
    
    # Define default arguments for spectrogram
    default = dict(
                nperseg=int(fs / df),
                noverlap=int(fs / df * overlap_frac),
                window='hanning', detrend='linear', return_onesided=True,
                scaling='density', mode='complex'
            )
    
    # Update default arguments if key words are given as input
    default.update(**kwargs)
    
    # Calculate spectrogram in the time domain
    f, t, s = spectrogram(Sig,fs,**default)
    
    # Pre-allocate s_kf
    shape = s.shape if len(x_ch) % 2 != 0 else (s.shape[0] + 1, s.shape[1], s.shape[2])
    s_kf = np.ndarray(shape)
    
    for i_t in range(len(t)):
        for i_f in range(len(f)):
            # select data
            data = s[:, i_f, i_t]
            # transform in the x domain
            k, psd = _k_dft_helper(x_ch, np.conj(data), window = 'hanning', mean = False)
            # save
            s_kf[:, i_f, i_t] = psd
    
    return k, f, t, s_kf