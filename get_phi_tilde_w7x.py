""" 
Provides functions for getting lines-of-sight (LoSs) and fluctuating phase shifts for the synthetic PCI diagnostic at W7-X

Uses the "geometry" class and the "vmec" module of the W7-X "PCIanalysis" library (avaialable at https://git.ipp-hgw.mpg.de/astechow/pcianalysis/-/tree/develop/python/PCIanalysis)
Output is given in the object plane (plane of the plasma), the laser LoS is assumed to be located at the origin/optical axis, the detector elements are assumed to be distributed
along the x-direction (but may have a finite extent along the y-direction, which is also perperdicular to the laser LoS). Physical quantities are in SI units unless explicitly stated otherwise.

Author: 
    - Soeren Kjer Hansen <kjerh@ipp.mpg.de>
"""

# Import the geometry class and the vmec module from PCIanalysis
import sys
sys.path.append("/home/IPP-HGW/kjerh/pcianalysis/python")
import PCIanalysis.geometry as geometry
import PCIanalysis.vmec as vmec

# Import standard modules
import numpy as np
from scipy.interpolate import interp1d, interpn
from scipy.integrate import simps, cumtrapz
import h5py
import pandas

def integration_paths_1d_confid(confid,aperture,num_paths,num_points = 1000):
    '''Get integration paths in W7-X Cartesian coordinates for calculating the fluctuating phase shift along the x-direction, as well as their lenghts
    
    Args:
        confid (integer): VMEC configuration ID for W7-X equilibrium
        aperture (float): half-width of the region covered by the LoSs along x
        num_paths (integer): number of integration paths
        num_points (integer): number of point along each integration path (defaults to 1000)
        
    Returns:
        Int_path1d (3d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths)
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
    '''
    
    # Extract the laser LoS in W7-X Cartesian coordinates
    geo = geometry.Geometry(confid)
    laser_los_cart = geo.los
    
    # Get shape of the laser LoS array
    shape_los = laser_los_cart.shape
    dim_los = shape_los[0]
    n_los = shape_los[1]
    
    # Define interpolation functions for W7-X Cartesian coordinates of the laser LoS
    x_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[0,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    y_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[1,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    z_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[2,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    
    # Interpolate points along the laser LoS to an LoS with the chosen number of points
    x_HiRes = x_los_cart_interp(np.linspace(0,n_los,num_points))
    y_HiRes = y_los_cart_interp(np.linspace(0,n_los,num_points))
    z_HiRes = z_los_cart_interp(np.linspace(0,n_los,num_points))
    
    # Get length of the LoSs
    dx_HiRes = np.diff(x_HiRes)
    dy_HiRes = np.diff(y_HiRes)
    dz_HiRes = np.diff(z_HiRes)
    ds = np.sqrt(dx_HiRes**2 + dy_HiRes**2 + dz_HiRes**2)
    s = np.zeros(x_HiRes.shape)
    s[1:] = np.cumsum(ds)
    
    # get unit vector of the x-direction in W7-X Cartesian coordinates
    e_measurement_angle = geo.get_e_m_for_angle()
    
    # Get displacement relative to the laser LoS along the x-direction
    x1d = np.linspace(-aperture,aperture,num_paths)
    
    # Pre-allocate Int_path1d
    Int_path1d = np.zeros((dim_los,num_points,num_paths))
    
    # Displace the laser LoS by the displacement of the paths along the x-direction
    for n in range(num_paths):
        Int_path1d[:,:,n] = np.asarray([x_HiRes + x1d[n]*e_measurement_angle[0],y_HiRes + x1d[n]*e_measurement_angle[1],z_HiRes + x1d[n]*e_measurement_angle[2]])
    
    return Int_path1d, s, x1d

def integration_paths_2d_confid(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points = 1000):
    '''Get integration paths in W7-X Cartesian coordinates for calculating the fluctuating phase shift along the x- and y-directions assuming square detector elements, as well as their lenghts
    
    Args:
        confid (integer): VMEC configuration ID for W7-X equilibrium
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        Magnification (float): (object size)/(image size)
        det_space (space): spacing of detectors in the image plane
        num_paths_x (integer): number of integration paths along x
        num_paths_y (integer): number of integration paths along y
        num_points (integer): number of point along each integration path (defaults to 1000)
        
    Returns:
        Int_path2d (4d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths_x,paths_y)
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        y1d (1d array): y-position of path in the object plane (the laser LoS is at y = 0)
    
    Notes:
        Magnification is defined as (object size)/(image size), which differs from some standard definitions
    '''
    
    # Extract the laser LoS in W7-X Cartesian coordinates
    geo = geometry.Geometry(confid)
    laser_los_cart = geo.los
    
    # Get shape of the laser LoS array
    shape_los = laser_los_cart.shape
    dim_los = shape_los[0]
    n_los = shape_los[1]
    
    # Define interpolation functions for W7-X Cartesian coordinates of the laser LoS
    x_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[0,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    y_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[1,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    z_los_cart_interp = interp1d(np.arange(n_los),laser_los_cart[2,:],kind = 'linear',bounds_error = False,fill_value = "extrapolate")
    
    # Interpolate points along the laser LoS to an LoS with the chosen number of points
    x_HiRes = x_los_cart_interp(np.linspace(0,n_los,num_points))
    y_HiRes = y_los_cart_interp(np.linspace(0,n_los,num_points))
    z_HiRes = z_los_cart_interp(np.linspace(0,n_los,num_points))
    
    # Get length of the LoSs
    dx_HiRes = np.diff(x_HiRes)
    dy_HiRes = np.diff(y_HiRes)
    dz_HiRes = np.diff(z_HiRes)
    ds = np.sqrt(dx_HiRes**2 + dy_HiRes**2 + dz_HiRes**2)
    s = np.zeros(x_HiRes.shape)
    s[1:] = np.cumsum(ds)
    
    # Get unit vector of the x-direction in W7-X Cartesian coordinates
    e_measurement_angle = geo.get_e_m_for_angle()
    # Get unit vector along the laser LoS in W7-X Cartesian coordinates
    e_l = geo.e_l
    # Get unit vector of the y-direction in W7-X Cartesian coordinates
    e_y = np.cross(e_l,e_measurement_angle)
    e_y = e_y/np.linalg.norm(e_y)
    
    # Get size of detector elements in the object plane
    obj_size = Magnification*det_space
    
    # Define diplacements along the x-direction
    x1d = np.linspace(-aperture,aperture,num_paths_x)
    
    # Define displacements along the y-direction
    y1d = np.linspace(-0.5*obj_size,0.5*obj_size,num_paths_y)
    
    # Pre-allocate Int_path2d
    Int_path2d = np.zeros((dim_los,num_points,num_paths_x,num_paths_y))
    
    # Displace the laser LoS by the displacement of path (n,m) along the (x,y)-directions in the object plane
    for n in range(num_paths_x):
        for m in range(num_paths_y):
            Int_path2d[:,:,n,m] = np.asarray([x_HiRes + x1d[n]*e_measurement_angle[0] + y1d[m]*e_y[0],y_HiRes + x1d[n]*e_measurement_angle[1] + y1d[m]*e_y[1],z_HiRes + x1d[n]*e_measurement_angle[2] +y1d[m]*e_y[2]])
    
    return Int_path2d, s, x1d, y1d

def integration_paths_1d_cyl(Int_path1d):
    '''Get integration paths in cylindrical coordinates for paths along the x-direction
    
    Args:
        Int_path1d (3d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths)
    
    Returns:
        Int_path1d_cyl (3d array): W7-X cylindrical coordinates of the integration paths (coord,points,paths)
    '''
    
    # Pre-allocate Int_path1d_cyl
    Int_path1d_cyl = np.zeros(Int_path1d.shape)
    
    # Calculate Int_path1d_cyl
    Int_path1d_cyl[0,:,:] = np.sqrt(Int_path1d[0,:,:]**2 + Int_path1d[1,:,:]**2)
    Int_path1d_cyl[1,:,:] = np.arctan2(Int_path1d[1,:,:],Int_path1d[0,:,:])
    Int_path1d_cyl[2,:,:] = Int_path1d[2,:,:]
    
    return Int_path1d_cyl

def integration_paths_2d_cyl(Int_path2d):
    '''Get integration paths in cylindrical coordinates for paths along the x- and y-directions
    
    Args:
        Int_path2d (4d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths_x,paths_y)
    
    Returns:
        Int_path2d_cyl (4d array): W7-X cylindrical coordinates of the integration paths (coord,points,paths_x,paths_y)
    '''
    
    # Pre-allocate Int_path2d_cyl
    Int_path2d_cyl = np.zeros(Int_path2d.shape)
    
    # Calculate Int_path1d_cyl
    Int_path2d_cyl[0,:,:] = np.sqrt(Int_path2d[0,:,:,:]**2 + Int_path2d[1,:,:,:]**2)
    Int_path2d_cyl[1,:,:] = np.arctan2(Int_path2d[1,:,:,:],Int_path2d[0,:,:,:])
    Int_path2d_cyl[2,:,:] = Int_path2d[2,:,:,:]
    
    return Int_path2d_cyl

def integration_paths_1d_vmec_confid(confid,Int_path1d):
    '''Get integration paths in VMEC coordinates (see http://webservices.ipp-hgw.mpg.de/docs/vmec.html#introduction) for paths along the x-direction
    
    Args:
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        Int_path1d (3d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths)
    
    Returns:
        Int_path1d_vmec (3d array): W7-X VMEC coordinates of the integration paths (coord,points,paths)
    '''
    
    # Pre-allocate Int_path1d_vmec
    array_shape = Int_path1d.shape
    Int_path1d_vmec = np.zeros(array_shape)
    
    # Calculate Int_path1d_vmec
    for n in range(array_shape[2]):
        s, theta, phi = vmec.get_VMEC_coords(*Int_path1d[:,:,n],confid)
        Int_path1d_vmec[:,:,n] = np.asarray([s,theta,phi])
    
    return Int_path1d_vmec

def integration_paths_2d_vmec_confid(confid,Int_path2d):
    '''Get integration paths in VMEC coordinates (see http://webservices.ipp-hgw.mpg.de/docs/vmec.html#introduction) for paths along the x- and y-directions
    
    Args:
        Int_path2d (4d array): W7-X Cartesian coordinates of the integration paths (coord,points,paths_x,paths_y)
    
    Returns:
        Int_path2d_vmec (4d array): W7-X VMEC coordinates of the integration paths (coord,points,paths_x,paths_y)
    '''
    
    # Pre-allocate Int_path2d_vmec
    array_shape = Int_path2d.shape
    Int_path2d_vmec = np.zeros(array_shape)
    
    # Calculate Int_path2d_vmec
    for n in range(array_shape[2]):
        for m in range(array_shape[3]):
            s, theta, phi = vmec.get_VMEC_coords(*Int_path2d[:,:,n,m],confid)
            Int_path2d_vmec[:,:,n,m] = np.asarray([s,theta,phi])
    
    return Int_path2d_vmec

def integration_paths_1d_vmec_confid_hi_res(confid,aperture,num_paths,num_points=1000):
    '''Get integration paths in VMEC coordinates (see http://webservices.ipp-hgw.mpg.de/docs/vmec.html#introduction) for paths along the x-direction with high resolution (web-interface
    bugs out for num_points > 133, so interpolation is required for higher resolution)
    
    Args:
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        num_paths (integer): number of integration paths
        num_points (integer): number of point along each integration path (defaults to 1000)
    
    Returns:
        Int_path1d_vmec (3d array): W7-X VMEC coordinates of the integration paths (coord,points,paths)
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
    '''
    
    # Get integration paths in Cartesian coordinates with a coarse resolution that can be handled by the web-interface
    Int_path1d_coarse, s_coarse, x1d = integration_paths_1d_confid(confid,aperture,num_paths,133)
    # Get integration paths in VMEC coordinates with a coarse resolution that can be handled by the web-interface
    Int_path1d_vmec_coarse = integration_paths_1d_vmec_confid(confid,Int_path1d_coarse)
    
    # Get integration paths in Cartesian coordinates with the desired resolution
    Int_path1d_fine, s, x1d = integration_paths_1d_confid(confid,aperture,num_paths,num_points)
    
    # Pre-allocate Int_path1d_vmec
    Int_path1d_vmec = np.zeros(Int_path1d_fine.shape)
    
    # Loop over integration paths
    for n in range(num_paths):
        # Find the first and last elements for which the VMEC coordinates are defined on the coarse grid
        Int_path_ser = pandas.Series(Int_path1d_vmec_coarse[0,:,n])
        m_first = Int_path_ser.first_valid_index()
        m_last = Int_path_ser.last_valid_index()
        
        # Find points where the poloidal angle jumps to ensure correct cyclical interpolation
        jump_down = np.where(np.nan_to_num(np.diff(Int_path1d_vmec_coarse[1,:,n])) < -np.pi)
        jump_up = np.where(np.nan_to_num(np.diff(Int_path1d_vmec_coarse[1,:,n])) > np.pi)
        jump_down = jump_down[0].tolist()
        jump_up = jump_up[0].tolist()
        
        # Loop over the poloidal angle jumps to create a smooth poloidal angle function
        ldown = len(jump_down)
        lup = len(jump_up)
        if jump_down:
            for m in range(ldown):
                Int_path1d_vmec_coarse[1,(jump_down[m]+1):,n] = np.nan_to_num(Int_path1d_vmec_coarse[1,(jump_down[m]+1):,n]) + 2*np.pi
        if jump_up:
            for l in range(lup):
                Int_path1d_vmec_coarse[1,(jump_up[l]+1):,n] = np.nan_to_num(Int_path1d_vmec_coarse[1,(jump_up[l]+1):,n]) - 2*np.pi
    
        # Define interpolation functions for the VMEC coordinates
        Int_path_interp_flux = interp1d(s_coarse[m_first:(m_last+1)],Int_path1d_vmec_coarse[0,m_first:(m_last+1),n],kind = 'cubic',bounds_error = False)
        Int_path_interp_theta = interp1d(s_coarse[m_first:(m_last+1)],Int_path1d_vmec_coarse[1,m_first:(m_last+1),n],kind = 'linear',bounds_error = False)
        Int_path_interp_phi = interp1d(s_coarse[m_first:(m_last+1)],Int_path1d_vmec_coarse[2,m_first:(m_last+1),n],kind = 'cubic',bounds_error = False)
        
        # Calculate Int_path1d_vmec and re-map the poloidal angle to the interval [0,2pi[
        Int_path1d_vmec[0,:,n] = Int_path_interp_flux(s)
        Int_path1d_vmec[1,:,n] = Int_path_interp_theta(s) % (2*np.pi)
        Int_path1d_vmec[2,:,n] = Int_path_interp_phi(s)
    
    return Int_path1d_vmec, s, x1d

def integration_paths_2d_vmec_confid_hi_res(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points=1000):
    '''Get integration paths in VMEC coordinates (see http://webservices.ipp-hgw.mpg.de/docs/vmec.html#introduction) for paths along the x- and y-directions with high resolution (web-interface bugs
    out for num_points > 133, so interpolation is required for higher resolution)
    
    Args:
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        Magnification (float): (object size)/(image size)
        det_space (space): spacing of detectors in the image plane
        num_paths_x (integer): number of integration paths along x
        num_paths_y (integer): number of integration paths along y
        num_points (integer): number of point along each integration path (defaults to 1000)
        
    Returns:
        Int_path2d (4d array): W7-X VMEC coordinates of the integration paths (coord,points,paths_x,paths_y)
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        y1d (1d array): y-position of path in the object plane (the laser LoS is at y = 0)
    
    Notes:
        Magnification is defined as (object size)/(image size), which differs from some standard definitions
    '''
    
    # Get integration paths in Cartesian coordinates with a coarse resolution that can be handled by the web-interface
    Int_path2d_coarse, s_coarse, x1d, y1d = integration_paths_2d_confid(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,133)
    # Get integration paths in VMEC coordinates with a coarse resolution that can be handled by the web-interface
    Int_path2d_vmec_coarse = integration_paths_2d_vmec_confid(confid,Int_path2d_coarse)
    
    # Get integration paths in Cartesian coordinates with the desired resolution
    Int_path2d_fine, s, x1d, y1d = integration_paths_2d_confid(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points)
    
    # Pre-allocate Int_path2d_vmec
    Int_path2d_vmec = np.zeros(Int_path2d_fine.shape)
    
    # Loop over integration paths
    for n in range(num_paths_x):
        for j in range(num_paths_y):
            # Find the first and last elements for which the VMEC coordinates are defined on the coarse grid
            Int_path_ser = pandas.Series(Int_path2d_vmec_coarse[0,:,n,j])
            m_first = Int_path_ser.first_valid_index()
            m_last = Int_path_ser.last_valid_index()
            
            # Find points where the poloidal angle jumps to ensure correct cyclical interpolation
            jump_down = np.where(np.nan_to_num(np.diff(Int_path2d_vmec_coarse[1,:,n,j])) < -np.pi)
            jump_up = np.where(np.nan_to_num(np.diff(Int_path2d_vmec_coarse[1,:,n,j])) > np.pi)
            jump_down = jump_down[0].tolist()
            jump_up = jump_up[0].tolist()
            
            # Loop over the poloidal angle jumps to create a smooth poloidal angle function
            ldown = len(jump_down)
            lup = len(jump_up)
            if jump_down:
                for m in range(ldown):
                    Int_path2d_vmec_coarse[1,(jump_down[m]+1):,n,j] = np.nan_to_num(Int_path2d_vmec_coarse[1,(jump_down[m]+1):,n,j]) + 2*np.pi
            if jump_up:
                for l in range(lup):
                    Int_path2d_vmec_coarse[1,(jump_up[l]+1):,n,j] = np.nan_to_num(Int_path2d_vmec_coarse[1,(jump_up[l]+1):,n,j]) - 2*np.pi
            
            # Define interpolation functions for the VMEC coordinates
            Int_path_interp_flux = interp1d(s_coarse[m_first:(m_last+1)],Int_path2d_vmec_coarse[0,m_first:(m_last+1),n,j],kind = 'cubic',bounds_error = False)
            Int_path_interp_theta = interp1d(s_coarse[m_first:(m_last+1)],Int_path2d_vmec_coarse[1,m_first:(m_last+1),n,j],kind = 'linear',bounds_error = False)
            Int_path_interp_phi = interp1d(s_coarse[m_first:(m_last+1)],Int_path2d_vmec_coarse[2,m_first:(m_last+1),n,j],kind = 'cubic',bounds_error = False)
            
            # Calculate Int_path2d_vmec and re-map the poloidal angle to the interval [0,2pi[
            Int_path2d_vmec[0,:,n,j] = Int_path_interp_flux(s)
            Int_path2d_vmec[1,:,n,j] = Int_path_interp_theta(s) % (2*np.pi)
            Int_path2d_vmec[2,:,n,j] = Int_path_interp_phi(s)
    
    return Int_path2d_vmec, s, x1d, y1d

def integration_paths_1d_vmec_to_sim(Int_path1d_vmec):
    '''Transform integration paths in VMEC coordinates to the PEST coordinates of a GENE-3D simulation (see M. Maurer et al. "GENE-3D: A Global Gyrokinetic Turbulence Code for Stellarators."
    Journal of Computational Physics Vol. 420, 109694 (2020)) for integration paths along the x-direction
    
    Args:
        Int_path1d_vmec (3d array): W7-X VMEC coordinates of the integration paths (coord,points,paths)
    
    Args:
        Int_path1d_sim (3d array): W7-X PEST coordinates of the integration paths from GENE-3D (coord,points,paths)
    '''
    
    # Pre-allocate Int_path1d_sim
    Int_path1d_sim = np.zeros(Int_path1d_vmec.shape)
    
    # Calculate Int_path1d_sim
    Int_path1d_sim[0,:,:] = np.sqrt(Int_path1d_vmec[0,:,:])
    Int_path1d_sim[1,:,:] = 4*2*np.pi/5 - Int_path1d_vmec[2,:,:]
    Int_path1d_sim[2,:,:] = Int_path1d_vmec[1,:,:] - 2*np.pi*(np.nan_to_num(Int_path1d_vmec[1,:,:])>np.pi)
    
    return Int_path1d_sim

def integration_paths_2d_vmec_to_sim(Int_path2d_vmec):
    '''Transform integration paths in VMEC coordinates to the PEST coordinates of a GENE-3D simulation (see M. Maurer et al. "GENE-3D: A Global Gyrokinetic Turbulence Code for Stellarators."
    Journal of Computational Physics Vol. 420, 109694 (2020)) for integration paths along the x- and y-directions
    
    Args:
        Int_path2d_vmec (4d array): W7-X VMEC coordinates of the integration paths (coord,points,paths_x,paths_y)
    
    Args:
        Int_path2d_sim (4d array): W7-X PEST coordinates of the integration paths from GENE-3D (coord,points,paths_x,paths_y)
    '''
    
    # Pre-allocate Int_path2d_sim
    Int_path2d_sim = np.zeros(Int_path2d_vmec.shape)
    
    # Calculate Int_path2d_sim
    Int_path2d_sim[0,:,:,:] = np.sqrt(Int_path2d_vmec[0,:,:,:])
    Int_path2d_sim[1,:,:,:] = 4*2*np.pi/5 - Int_path2d_vmec[2,:,:,:]
    Int_path2d_sim[2,:,:,:] = Int_path2d_vmec[1,:,:,:] - 2*np.pi*(np.nan_to_num(Int_path2d_vmec[1,:,:,:])>np.pi)
    
    return Int_path2d_sim

def get_ne_tilde_GENE3D_PEST(filename):
    '''Load electron density fluctuations from a GENE-3D simulation .h5-file
    
    Args:
        filname (string): path of the GENE-3D .h5-file
    
    Returns:
        t (1d array): time points of the GENE-3D simulation
        rho (1d array): square root of the normalized toroidal flux (radial PEST coordinate)
        phi (1d array): toroidal angle (toroidal PEST coordinate)
        theta (1d array): straight field-line poloidal angle (poloidal PEST coordinate)
        ne_tilde (4d array): fluctuating electron density on the (t,rho,phi,theta)-grid
    '''
    
    # Load data from .h5-file
    with h5py.File(filename,'r') as hf:
        time = np.array(hf.get('/times'))
        rho = np.array(hf.get('/x_a'))
        phi = np.array(hf.get('/phi'))
        theta = np.array(hf.get('/theta'))
        ne_tilde = np.array(hf.get('/field'))
    
    # Rearrange phi- and theta-directions, such that the are increasing
    theta = np.flip(theta)
    phi = np.flip(phi)
    ne_tilde = np.flip(ne_tilde,3)
    ne_tilde = np.flip(ne_tilde,2)
    
    # Convert time from microseconds to seconds
    t = 1e-6*time

    return t, rho, phi, theta, ne_tilde

def get_phi_tilde_1d_PEST(filename,confid,aperture,num_paths,num_points = 1000,wavelen = 10.6e-6):
    '''Get the fluctuating phase shift along the x-direction from a GENE-3D simulation by interpolating over the regular PEST-grid
    
    Args:
        filename (string): path of the GENE-3D .h5-file
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        num_paths (integer): number of integration paths
        num_points (integer): number of point along each integration path (defaults to 1000)
        wavelen (float): wavelength of the laser (defaults to 10.6e-6 m, which is the wavelength of a CO2 laser)
        
    Returns:
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        t (1d array): time points of the simulation
        phi_tilde (2d array): fluctuating phase shift (x1d,t)
    '''
    
    # Load fluctuating electron density from the GENE-3D simulation .h5-file
    t, rho, phi, theta, ne_tilde = get_ne_tilde_GENE3D_PEST(filename)
    
    # Get integration paths in VMEC coordinates
    Int_path1d_vmec, s, x1d = integration_paths_1d_vmec_confid_hi_res(confid,aperture,num_paths,num_points)
    # Convert integration paths to the PEST coordinates of the GENE-3D simulation
    Int_path1d_sim = integration_paths_1d_vmec_to_sim(Int_path1d_vmec)
    
    # Pre-allocate Integ
    lt = len(t)
    Integ = np.zeros((num_paths,lt))
    
    # Loop over t
    for n in range(lt):
        # Extract the fluctuating electron density at time point n
        ne_tilde_interp = ne_tilde[n,:,:,:]
        # Interpolate the fluctuating electron density from the simulation grid to the integration paths
        ne_tilde_Int = interpn((rho,phi,theta),ne_tilde_interp,(Int_path1d_sim[0,:,:],Int_path1d_sim[1,:,:],Int_path1d_sim[2,:,:]),method='linear',bounds_error = False)
        # Convert nan elements to zeros
        ne_tilde_Int_0 = np.nan_to_num(ne_tilde_Int)
        
        # Integrate the fluctuating electron density over the integration paths
        for l in range(num_paths):
            Integ[l,n] = simps(ne_tilde_Int_0[:,l],s)
    
    # Define the classical electron radius
    rec = 2.81794e-15
    
    # Calculate fluctuating phase shift
    phi_tilde = -rec*wavelen*Integ
    
    return x1d, t, phi_tilde

def get_phi_tilde_2d_PEST(filename,confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points = 1000,wavelen = 10.6e-6):
    '''Get the fluctuating phase shift along the x- and y-directions from a GENE-3D simulation by interpolating over the regular PEST-grid
    
    Args:
        filename (string): path of the GENE-3D .h5-file
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        Magnification (float): (object size)/(image size)
        det_space (space): spacing of detectors in the image plane
        num_paths_x (integer): number of integration paths along x
        num_paths_y (integer): number of integration paths along y
        num_points (integer): number of points along each integration path (defaults to 1000)
        wavelen (float): wavelength of the laser (defaults to 10.6e-6 m, which is the wavelength of a CO2 laser)
        
    Returns:
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        y1d (1d array): y-position of path in the object plane (the laser LoS is at y = 0)
        t (1d array): time points of the simulation
        phi_tilde (3d array): fluctuating phase shift (x1d,y1d,t)
    '''
    
    # Load fluctuating electron density from the GENE-3D simulation .h5-file
    t, rho, phi, theta, ne_tilde = get_ne_tilde_GENE3D_PEST(filename)
    
    # Get integration paths in VMEC coordinates
    Int_path2d_vmec, s, x1d, y1d = integration_paths_2d_vmec_confid_hi_res(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points)
    # Convert integration paths to the PEST coordinates of the GENE-3D simulation
    Int_path2d_sim = integration_paths_2d_vmec_to_sim(Int_path2d_vmec)
    
    # Pre-allocate Integ
    lt = len(t)
    Integ = np.zeros((num_paths_x,num_paths_y,lt))
    
    # Loop over t
    for n in range(lt):
        # Extract the fluctuating electron density at time point n
        ne_tilde_interp = ne_tilde[n,:,:,:]
        # Interpolate the fluctuating electron density from the simulation grid to the integration paths
        ne_tilde_Int = interpn((rho,phi,theta),ne_tilde_interp,(Int_path2d_sim[0,:,:,:],Int_path2d_sim[1,:,:,:],Int_path2d_sim[2,:,:,:]),method='linear',bounds_error = False)
        # Convert nan elements to zeros
        ne_tilde_Int_0 = np.nan_to_num(ne_tilde_Int)
        
        # Integrate the fluctuating electron density over the integration paths
        for l in range(num_paths_x):
            for m in range(num_paths_y):
                Integ[l,m,n] = simps(ne_tilde_Int_0[:,l,m],s)
    
    # Define the classical electron radius
    rec = 2.81794e-15
    
    # Calculate fluctuating phase shift
    phi_tilde = -rec*wavelen*Integ
    
    return x1d, y1d, t, phi_tilde

def get_phi_tilde_cum_1d_PEST(filename,confid,aperture,num_paths,num_points = 1000,wavelen = 10.6e-6):
    '''Get the cumulative fluctuating phase shift along the x-direction from a GENE-3D simulation by interpolating over the regular PEST-grid
    
    Args:
        filename (string): path of the GENE-3D .h5-file
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        num_paths (integer): number of integration paths
        num_points (integer): number of point along each integration path (defaults to 1000)
        wavelen (float): wavelength of the laser (defaults to 10.6e-6 m, which is the wavelength of a CO2 laser)
        
    Returns:
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        t (1d array): time points of the simulation
        phi_tilde_cum (3d array): cumulative fluctuating phase shift (s,x1d,t)
    '''
    
    # Load fluctuating electron density from the GENE-3D simulation .h5-file
    t, rho, phi, theta, ne_tilde = get_ne_tilde_GENE3D_PEST(filename)
    
    # Get integration paths in VMEC coordinates
    Int_path1d_vmec, s, x1d = integration_paths_1d_vmec_confid_hi_res(confid,aperture,num_paths,num_points)
    # Convert integration paths to the PEST coordinates of the GENE-3D simulation
    Int_path1d_sim = integration_paths_1d_vmec_to_sim(Int_path1d_vmec)
    
    # Pre-allocate Integ
    lt = len(t)
    Integ = np.zeros((num_points,num_paths,lt))
    
    # Loop over t
    for n in range(lt):
        # Extract the fluctuating electron density at time point n
        ne_tilde_interp = ne_tilde[n,:,:,:]
        # Interpolate the fluctuating electron density from the simulation grid to the integration paths
        ne_tilde_Int = interpn((rho,phi,theta),ne_tilde_interp,(Int_path1d_sim[0,:,:],Int_path1d_sim[1,:,:],Int_path1d_sim[2,:,:]),method='linear',bounds_error = False)
        # Convert nan elements to zeros
        ne_tilde_Int_0 = np.nan_to_num(ne_tilde_Int)
        
        # Calculate cumulative integral of the fluctuating electron density over the integration paths
        for l in range(num_paths):
            Integ[:,l,n] = cumtrapz(ne_tilde_Int_0[:,l],s,initial = True)
    
    # Define the classical electron radius
    rec = 2.81794e-15
    
    # Calculate fluctuating phase shift
    phi_tilde_cum = -rec*wavelen*Integ
    
    return s, x1d, t, phi_tilde_cum

def get_phi_tilde_cum_2d_PEST(filename,confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points = 1000,wavelen = 10.6e-6):
    '''Get the cumulative fluctuating phase shift along the x- and y-directions from a GENE-3D simulation by interpolating over the regular PEST-grid
    
    Args:
        filename (string): path of the GENE-3D .h5-file
        confid (integer): VMEC configuration ID for W7-X equilibrium 
        aperture (float): half-width of the region covered by the LoSs in the measurement direction
        Magnification (float): (object size)/(image size)
        det_space (space): spacing of detectors in the image plane
        num_paths_x (integer): number of integration paths along x
        num_paths_y (integer): number of integration paths along y
        num_points (integer): number of points along each integration path (defaults to 1000)
        wavelen (float): wavelength of the laser (defaults to 10.6e-6 m, which is the wavelength of a CO2 laser)
        
    Returns:
        s (1d array): length along LoSs
        x1d (1d array): x-position of path in the object plane (the laser LoS is at x = 0)
        y1d (1d array): y-position of path in the object plane (the laser LoS is at y = 0)
        t (1d array): time points of the simulation
        phi_tilde_cum (4d array): cumulative fluctuating phase shift (s,x1d,y1d,t)
    '''
    
    # Load fluctuating electron density from the GENE-3D simulation .h5-file
    t, rho, phi, theta, ne_tilde = get_ne_tilde_GENE3D_PEST(filename)
    
    # Get integration paths in VMEC coordinates
    Int_path2d_vmec, s, x1d, y1d = integration_paths_2d_vmec_confid_hi_res(confid,aperture,Magnification,det_space,num_paths_x,num_paths_y,num_points)
    # Convert integration paths to the PEST coordinates of the GENE-3D simulation
    Int_path2d_sim = integration_paths_2d_vmec_to_sim(Int_path2d_vmec)
    
    # Pre-allocate Integ
    lt = len(t)
    Integ = np.zeros((num_points,num_paths_x,num_paths_y,lt))
    
    # Loop over t
    for n in range(lt):
        # Extract the fluctuating electron density at time point n
        ne_tilde_interp = ne_tilde[n,:,:,:]
        # Interpolate the fluctuating electron density from the simulation grid to the integration paths
        ne_tilde_Int = interpn((rho,phi,theta),ne_tilde_interp,(Int_path2d_sim[0,:,:,:],Int_path2d_sim[1,:,:,:],Int_path2d_sim[2,:,:,:]),method='linear',bounds_error = False)
        # Convert nan elements to zeros
        ne_tilde_Int_0 = np.nan_to_num(ne_tilde_Int)
        
        # Calculate cumulative integral of the fluctuating electron density over the integration paths
        for l in range(num_paths_x):
            for m in range(num_paths_y):
                Integ[:,l,m,n] = cumtrapz(ne_tilde_Int_0[:,l,m],s,initial = True)
    
    # Define the classical electron radius
    rec = 2.81794e-15
    
    # Calculate fluctuating phase shift
    phi_tilde_cum = -rec*wavelen*Integ
    
    return s, x1d, y1d, t, phi_tilde_cum